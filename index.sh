#Update system
sudo pacman -Syyu                     --noconfirm --needed

#Install usefull packages
sudo pacman -S base-devel yaourt      --noconfirm --needed
sudo pacman -S chromium               --noconfirm --needed
sudo pacman -S xtrlock                --noconfirm --needed
sudo yaourt -S spotify                --noconfirm --needed
sudo pacman -S simple-scan            --noconfirm --needed
sudo pacman -S rhythmbox              --noconfirm --needed


#Remove stupid packages
sudo  pacman -R thunderbird           --noconfirm
sudo  pacman -R pidgin-libnotify      --noconfirm
sudo  pacman -R pidgin                --noconfirm
sudo  pacman -R hexchat               --noconfirm
sudo  pacman -R pragha                --noconfirm
sudo  pacman -R manjaro-hello         --noconfirm
sudo  pacman -R nemo-bulk-rename      --noconfirm
sudo  pacman -R gprename              --noconfirm
sudo  pacman -R kvantum-manjaro       --noconfirm
sudo  pacman -R kvantum-qt5           --noconfirm
sudo  pacman -R uxterm                --noconfirm


#Appearance
sudo  mkdir   /usr/share/backgrounds/carlos/
sudo  cp -a   ./wallpaper.jpg                   /usr/share/backgrounds/carlos/wallpaper.jpg     #Wallpaper
sudo  cp -a   ./ePapirus                        /usr/share/icons/ePapirus                       #Icons
sudo  cp -a   ./Carta                           /usr/share/themes/Carta                         #Desktop
      gsettings set org.cinnamon.desktop.background picture-uri   "file:///usr/share/backgrounds/carlos/wallpaper.jpg"  #Wallpaper
      gsettings set org.cinnamon.desktop.interface icon-theme     "ePapirus"                                            #Icons
      gsettings set org.cinnamon.theme name                       "Carta"                                               #Desktop
      gsettings set org.cinnamon.desktop.interface gtk-theme      "Adapta-Nokto"                                        #Controls
      gsettings set org.cinnamon.desktop.wm.preferences theme     "Adapta-Nokto"                                        #Window borders



#Add to menu
sudo  cp -a   ./xtrlock-carlos-made.desktop     /usr/share/applications

#Clean form menu
      cd      /usr/share/applications
sudo  mkdir   obsolete

sudo  mv      ./avahi-discover.desktop          ./obsolete
sudo  mv      ./bssh.desktop                    ./obsolete
sudo  mv      ./bvnc.desktop                    ./obsolete
sudo  mv      ./xterm.desktop                   ./obsolete
sudo  mv      ./uxterm.desktop                  ./obsolete
sudo  mv      ./xsane.desktop                   ./obsolete
sudo  mv      ./pamac-updater.desktop           ./obsolete
sudo  mv      ./org.gnome.baobab.desktop        ./obsolete
sudo  mv      ./manjaro-documentation.desktop   ./obsolete
sudo  mv      ./cinnamon-settings-*             ./obsolete

      cd      obsolete
sudo  find . -name "*.desktop" -exec sh -c 'mv "$1" "${1%.desktop}.obsolete"' _ {} \;
